package controller;

import dao.PersonDao;
import model.Person;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class PersonController {

    @Resource
    PersonDao dao;

    @GetMapping("customers")
    public List<Person> getAllPersons(){

        return dao.getPersons();
    }

    @RequestMapping(value = "customers/{id}", method = RequestMethod.GET)
    @Transactional
    public Person getPerson(@PathVariable long id){
        return dao.getPersonById(id);
    }


    @RequestMapping(value = "customers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public void postPerson(@RequestBody @Valid Person person){
        dao.insertPerson(person);
    }



    @RequestMapping(value = "customers", method = RequestMethod.DELETE)
    @Transactional
    public void deleteAllPersons(){
        dao.deletePersons();
    }

    @RequestMapping(value = "customers/{id}", method = RequestMethod.DELETE)
    @Transactional
    public void deleteById(@PathVariable long id){
        dao.deletePersonById(id);
    }

    @GetMapping("customers/search")
    @Transactional
    public List<Person> search(
            @RequestParam(defaultValue = "") String key) {
        return dao.search(key);
    }

}
