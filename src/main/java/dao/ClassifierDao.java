package dao;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Mart on 10/23/2016.
 */
@Repository
public class ClassifierDao {
    public List<String> getCustomerTypes(){
        List<String> list = Arrays.asList("customer_type.private","customer_type.corporate");
        return list;
    }
    public List<String> getPhoneTypes(){
        List<String> list = Arrays.asList("phone_type.fixed","phone_type.mobile");
        return list;
    }
}
