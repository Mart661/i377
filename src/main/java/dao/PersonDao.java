package dao;

import org.springframework.stereotype.Repository;
import model.Person;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.util.*;

@Repository
public class PersonDao {
    @PersistenceContext
    private EntityManager em;

    public void deletePersonById(long id) throws RuntimeException{
        em.createQuery(("delete from Person p where p.id = :id"))
                .setParameter("id", id)
                .executeUpdate();
    }

    public Person getPersonById(long id) throws RuntimeException {
        return em.createQuery("SELECT p FROM Person p WHERE p.id = :id", Person.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public void insertPerson(Person person){
        em.persist(person);

    }
    public List<Person> getPersons() throws RuntimeException {
        return em.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    }

    public void deletePersons() throws RuntimeException{
        em.createQuery("DELETE FROM Phone").executeUpdate();
        em.createQuery("DELETE FROM Person").executeUpdate();
    }

    public List<Person> search(String searchTerm){
        return em.createQuery("SELECT p FROM Person p WHERE LOWER(p.firstName) LIKE :keyword " +
        "OR LOWER(p.lastName) LIKE :keyword OR LOWER(p.code) LIKE :keyword", Person.class)
                .setParameter("keyword", "%"+searchTerm+"%").getResultList();
    }
}

