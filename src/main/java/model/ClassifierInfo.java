package model;
import lombok.Data;

import java.util.List;

/**
 * Created by Mart on 10/19/2016.
 */
@Data
public class ClassifierInfo {
    private List<String> phoneTypes;
    private List<String> customerTypes;

    public ClassifierInfo(List<String> phoneTypes, List<String> customerTypes) {
        this.phoneTypes = phoneTypes;
        this.customerTypes = customerTypes;
    }
}
