package model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Person {
    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private long id;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    @Size(min = 2, max = 15)
    private String firstName;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    @Size(min = 2, max = 15)
    private String lastName;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    @Size(min = 2, max = 15)
    private String code;
    private String type;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "person_id", nullable = false)
    private List<Phone> phones = new ArrayList<>();

    public Person() {

    }


    public String fieldDataToString() {
        return firstName +" "+
                lastName +" "+
                code +" "+
               phones;
    }
}